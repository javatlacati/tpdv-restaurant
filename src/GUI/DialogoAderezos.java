/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Pedidos.Platillo;
import SistemaDeKioscos.ServicioDeAccesoABaseDeDatos;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Formatter;
import java.util.Hashtable;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 *
 * @author Administrador
 */
public class DialogoAderezos extends javax.swing.JFrame {

    ServicioDeAccesoABaseDeDatos baseDeDatos;
    Platillo platilloSeleccionado;
    String catego = "carnes";

    /**
     * Creates new form DialogoAderezos
     */
    public DialogoAderezos() {
        initComponents();
    }

    public DialogoAderezos(ServicioDeAccesoABaseDeDatos baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
        initComponents();
    }

    public DialogoAderezos(ServicioDeAccesoABaseDeDatos baseDeDatos, String catego) {
        this.baseDeDatos = baseDeDatos;
        this.catego = catego;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabSopas = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        pnlSopas = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tabSopas.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                tabSopasComponentShown(evt);
            }
        });
        tabSopas.setLayout(new java.awt.BorderLayout());

        pnlSopas.setLayout(new javax.swing.BoxLayout(pnlSopas, javax.swing.BoxLayout.Y_AXIS));
        jScrollPane1.setViewportView(pnlSopas);

        tabSopas.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        getContentPane().add(tabSopas, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tabSopasComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_tabSopasComponentShown
        System.out.println("Obteniendo aderezos de la categoría: " + catego);
        Hashtable<String, Double[]> datosplatillos = obtenerPlatillos(catego);
    }//GEN-LAST:event_tabSopasComponentShown

    private Hashtable<String, Double[]> obtenerPlatillos(String catego) {
        System.out.println("mostrando " + catego);
        Hashtable<String, Double[]> datosplatillos = new Hashtable<String, Double[]>();
        catego = catego.toLowerCase();
        //TODO para practicidad las imágenes de los patillos si las hay guardarlas con el mismo nombre del platillo y con extensión jpeg
        if (catego == "carnes") {
            baseDeDatos.obtenerPlatillosPorCategoría("aderezos_para_carne", datosplatillos);
        } else {
            //catego == "cortes"
            baseDeDatos.obtenerPlatillosPorCategoría("guarnicion_para_cortes", datosplatillos);
        }
        //TODO renderizar imágenes de los platillos en el panel
        if (catego == "carnes" || catego == "cortes") {
            pnlSopas.removeAll();
        } else {
            System.err.println("oops");
        }

        for (Map.Entry<String, Double[]> platillito : datosplatillos.entrySet()) {
            String nombrecito = platillito.getKey();
            System.out.println(nombrecito);
            Double[] precios = platillito.getValue();
            Formatter formatoNumero = new Formatter();

            //System.out.println("$" + precios);
            System.out.println(precios.length);

            //parte común
            PnlPlatillo panelPlatillo = new PnlPlatillo();
            panelPlatillo.setNombrePlatillo(nombrecito);
            //System.out.println((""));
            System.out.println(String.format("Precio:$\t%.2f", precios[1]));
            panelPlatillo.setPrecio(precios[1]);
            System.out.println("IVA" + precios[2]);
            panelPlatillo.setIva(precios[2]);
            panelPlatillo.setCategoria(catego);

            //    jScrollPane1.add(panelPlatillo);
            panelPlatillo.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    PnlPlatillo panel = (PnlPlatillo) e.getComponent();
                    platilloSeleccionado = new Platillo();
                    platilloSeleccionado.setNombrePlatillo(panel.getNombrePlatillo());
                    platilloSeleccionado.setPrecio(panel.getPrecio());
                    platilloSeleccionado.setIva(panel.getIva());
                    platilloSeleccionado.setCategoria(panel.getCategoria());

                    System.out.println("SEleccionado:" + platilloSeleccionado.getNombrePlatillo());
                    int opcion = JOptionPane.showConfirmDialog(null, "¿Desea Agregar " + platilloSeleccionado.getNombrePlatillo() + "?");
                    switch (opcion) {
                        case JOptionPane.YES_OPTION:
                            System.out.println("Usted ha elegido sí");
                            String canti = JOptionPane.showInputDialog("¿Cuantos " + panel.getNombrePlatillo() + " desea agregar?");
                            if (canti != "") {
                                int cantidadDePlatillos = 1;
                                cantidadDePlatillos = Integer.parseInt(canti);
                                platilloSeleccionado.setCantidad(cantidadDePlatillos);
                                //TODO la cantidad puede ser por defecto uno
                            }//en cualquier error dejamos solamente uno
                            break;
                        default:
                            System.out.println("Usted ha elegido no");
                            break;
                    };
                    //TODO para el caso de ensaladas preguntar por aderezos
                }
            });

            if (catego == "carnes" || catego == "cortes") {
                pnlSopas.add(panelPlatillo);
            } else {
                System.err.println("oops");
            }

        }
        return datosplatillos;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogoAderezos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogoAderezos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogoAderezos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogoAderezos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new DialogoAderezos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pnlSopas;
    private javax.swing.JPanel tabSopas;
    // End of variables declaration//GEN-END:variables
}
