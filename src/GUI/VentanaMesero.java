/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import GUI.touchscreen.VentanaLoginTouchScreen;
import Administracion.Mesero;
import Pedidos.Comanda;
import Pedidos.Mesa;
import SistemaDeKioscos.ServicioDeAccesoABaseDeDatos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import org.jvnet.substance.SubstanceLookAndFeel;
import org.jvnet.substance.api.SubstanceConstants;
import org.jvnet.substance.api.SubstanceSkin;
import org.jvnet.substance.skin.BusinessBlueSteelSkin;
import org.jvnet.substance.watermark.SubstanceImageWatermark;

/**
 *
 * @author Administrador
 */
public class VentanaMesero extends javax.swing.JFrame {

    Mesero mesero = new Mesero();
    ArrayList<Mesa> mesas = new ArrayList<Mesa>();
    JFrame login;//VentanaLoginTouchScreen
    private boolean contandoTiempo = false;
    Thread temporizadorLogin;
    ServicioDeAccesoABaseDeDatos baseDeDatos;// = new Servic    ioDeAccesoABaseDeDatos();

    public VentanaMesero(VentanaLoginTouchScreen loginw) {
        this.login = loginw;
        initComponents();
        //TODO hacer que las mesas cambien de color si están ocupadas
    }

    class miAdaptador extends MouseAdapter {

        private Mesa mesa;
        private VentanaMesero ventana;

        private miAdaptador(Mesa mesa, VentanaMesero ventana) {
            this.mesa = mesa;
            this.ventana = ventana;
        }

        @Override
        public void mouseClicked(java.awt.event.MouseEvent evt) {
            //mesa1MouseClicked(evt);
            //consultar en BD si la mesa esta ocupada
            int numeroDeMesa = mesa.getNumeroDeMesa();
            int estadomesa = baseDeDatos.compruebaEstadoMesa(numeroDeMesa, mesero);

            if (estadomesa != ServicioDeAccesoABaseDeDatos.SINDATOS) {
                if (estadomesa == ServicioDeAccesoABaseDeDatos.MESA_DESOCUPADA) {//comprobar en la BD si la mesa ya tenía cuentas abierta
                    cargaNuevaCartaALaMesa(numeroDeMesa);
                    System.out.println("Cargando una nueva carta");
                    ventanaDeCarta carta = new ventanaDeCarta(1, ventana);
                    carta.setMesero(mesero);
                    carta.setVisible(true);
                } else { //si no
                    System.out.println("Verificando que la mesa esté reservada por nosotros");
                    //TODO verificar que la mesa la tenga reservada el mismo mesero
                    if (baseDeDatos.comprobarSiAtiendeMesa(mesero, numeroDeMesa)) {
                        System.out.println("La mesa estaba reservada por nosotros");
                        System.out.println("Cargando carta anterior...");
                        Comanda carta = baseDeDatos.obtenerComandaExistenteDeMesero(numeroDeMesa, mesero);
                        ventanaDeCarta deCarta = new ventanaDeCarta(numeroDeMesa, ventana);
                        deCarta.setOrden(carta);
                        deCarta.setMesero(mesero);
                        deCarta.setVisible(true);
                        setVisible(false);
                    } else {
                        System.out.println("La mesa estaba reservada por alguien más");
                        JOptionPane.showMessageDialog(ventana, "Lo siento pero esta mesa ya está siendo atendida por alguien más");
                    }
                }
                //si ya ignorar, no podrá tocar una mesa usada por alguien más
            } else {
                System.err.println("No se pudo verificar posiblemente base de datos dañada");
            }
        }

    }

    public VentanaMesero(VentanaLoginMesero loginw) {
        this.login = loginw;
        initComponents();
        //TODO hacer que las mesas cambien de color si están ocupadas
    }

    public VentanaMesero(JFrame login, ServicioDeAccesoABaseDeDatos baseDeDatos) {
        this.login = login;
        this.baseDeDatos = baseDeDatos;
        Vector<Mesa> mesas = baseDeDatos.obtenerMesas();
        for (int i = 0; i < mesas.size(); i++) {
            Mesa mesa = mesas.elementAt(i);
            JLabel mesa1 = new JLabel();
            mesa1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
            mesa1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/res/mesas beta de 60x80 cms mesas en restaurante.jpg"))); // NOI18N
            mesa1.setText(mesa.getNumeroDeMesa() + "");
            mesa1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
            mesa1.addMouseListener(new miAdaptador(mesa, this));
            panelMesas.add(mesa1);
            //mesa1.setVisible(true);
            //pack();
        }
    }

    @Override
    public void setVisible(boolean b) {
        super.setVisible(b);
        SubstanceImageWatermark watermark = new SubstanceImageWatermark("GUI/res/texture_33_by_Sirius_sdz.jpg");
        //SubstanceCrosshatchWatermark watermark = new SubstanceCrosshatchWatermark();
        watermark.setKind(SubstanceConstants.ImageWatermarkKind.APP_TILE);
        watermark.setOpacity(0.2f);// 设置水印透明度          
        SubstanceSkin skin = new BusinessBlueSteelSkin()//CremeCoffeeSkin()
                .withWatermark(watermark); // 初始化有水印的皮肤
        SubstanceLookAndFeel.setSkin(skin);
        Vector<Mesa> mesas = baseDeDatos.obtenerMesas();
        for (int i = 0; i < mesas.size(); i++) {
            Mesa mesa = mesas.elementAt(i);
            JLabel mesa1 = new JLabel();
            mesa1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
            mesa1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/res/mesas beta de 60x80 cms mesas en restaurante.jpg"))); // NOI18N
            mesa1.setText(mesa.getNumeroDeMesa() + "");
            mesa1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
            mesa1.addMouseListener(new miAdaptador(mesa, this));
            panelMesas.add(mesa1);
            //mesa1.setVisible(true);
            //pack();
        }
    }

    public ServicioDeAccesoABaseDeDatos getBaseDeDatos() {
        return baseDeDatos;
    }

    public void setBaseDeDatos(ServicioDeAccesoABaseDeDatos baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
    }

    ActionListener actListner = new ActionListener() {

        @Override

        public void actionPerformed(ActionEvent event) {

            if (true == contandoTiempo) {
                //System.out.println("verificando...");
                long tiempoInactivo;
                Date cur_time = new Date();
                tiempoInactivo = cur_time.getTime() - last_act_time.getTime();
                //System.out.println("tiempo inactivo" + tiempoInactivo);
                if (tiempoInactivo >= 7000) { //1min
                    System.out.println("inactivo!");
                    contandoTiempo = false;
                    volverALogin();
                }
            } else {
                last_act_time = new Date();
            }

        }

    };

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblBarra = new javax.swing.JLabel();
        btnSalir = new javax.swing.JButton();
        lblCaja = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        panelMesas = new javax.swing.JPanel();

        setTitle("Creación de comandas");
        addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                formFocusGained(evt);
            }
        });

        lblBarra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/res/6201_l.jpg"))); // NOI18N
        lblBarra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblBarraMouseClicked(evt);
            }
        });

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        lblCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/res/registradora.png"))); // NOI18N
        lblCaja.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblCajaMouseClicked(evt);
            }
        });

        jScrollPane1.setViewportView(panelMesas);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(138, 138, 138)
                                .addComponent(lblCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(lblBarra, javax.swing.GroupLayout.PREFERRED_SIZE, 368, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(55, 55, 55)
                                .addComponent(btnSalir)))
                        .addGap(0, 116, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(lblBarra)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSalir)
                        .addGap(62, 62, 62)))
                .addComponent(lblCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblBarraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblBarraMouseClicked
        //cargaNuevaCartaALaMesa(-1);
        //TODO: cargar una carta especial apra la barra
    }//GEN-LAST:event_lblBarraMouseClicked

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        volverALogin();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void formFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_formFocusGained
        //registrarMovimiento();
        System.out.println("Ventana Mesero ganó el foco");
    }//GEN-LAST:event_formFocusGained

    private void lblCajaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCajaMouseClicked
//TODO calcular las ventas por mesero (opcional)
//TODO hacer ventana de cajero        

    }//GEN-LAST:event_lblCajaMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSalir;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblBarra;
    private javax.swing.JLabel lblCaja;
    private javax.swing.JPanel panelMesas;
    // End of variables declaration//GEN-END:variables

    Date last_act_time;

    public void registrarMovimiento() {
        last_act_time = new Date();
        // contandoTiempo = true;
        System.out.println("movimiento registrado");
    }

    void volverALogin() {
        this.dispose();
        contandoTiempo = false;
        login.setVisible(true);
    }

    public void correTiempo() {
        //    System.out.println("Iniciando");
        contandoTiempo = true;
        Timer timer = new Timer(500, actListner);

        timer.start();
        registrarMovimiento();
    }

    private void cargaNuevaCartaALaMesa(int i) {
        //    registrarMovimiento();
        baseDeDatos.atenderMesa(mesero, i);
        ventanaDeCarta carta;
        //mandamos esta ventana como referencia para que pueda reactivar el temporizador cuando se cierre
        carta = new ventanaDeCarta(i, this);
        carta.setVisible(true);
        contandoTiempo = false;
        System.out.println("Tiempo parado");
    }

    public void setMesero(Mesero m) {
        this.mesero = m;
    }

    public Mesero getMesero() {
        return mesero;
    }

}
