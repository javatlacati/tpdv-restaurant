package Administracion;


// <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
// #[regen=yes,id=DCE.DDD619C2-EF40-0696-0954-C05647E5F80B]
// </editor-fold> 
public class Cocinero {

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.02EA991C-BACA-71C8-A5BA-1704E9228FD7]
    // </editor-fold> 
    private String nombre;

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,id=DCE.9EF4173B-6AC7-02D7-969D-E9872644DB8D]
    // </editor-fold> 
    public Cocinero () {
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.6C48B25C-F6E6-4E39-2844-BB5A2D444731]
    // </editor-fold> 
    public String getNombre () {
        return nombre;
    }

    // <editor-fold defaultstate="collapsed" desc=" UML Marker "> 
    // #[regen=yes,regenBody=yes,id=DCE.A1CA12C3-36CF-DF0E-D207-FCCBC3E83F0E]
    // </editor-fold> 
    public void setNombre (String val) {
        this.nombre = val;
    }

}

