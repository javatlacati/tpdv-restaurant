package Cotizaciones;

/**
 * Cliente es cualquier cliente del cual guardemos los datos de factuación
 */
public class Cliente {
//TODO crearle su contraparte en la BD

    String nombreORazonSocial;
    String RFC;
    String CP;
    String Direccion;
    String Estado;
    String Municipio;

    public Cliente(String nombreORazonSocial, String RFC, String CP, String Direccion, String Estado, String Municipio) {
        this.nombreORazonSocial = nombreORazonSocial;
        this.RFC = RFC;
        this.CP = CP;
        this.Direccion = Direccion;
        this.Estado = Estado;
        this.Municipio = Municipio;
    }
    
    public String getNombreORazonSocial() {
        return nombreORazonSocial;
    }

    public void setNombreORazonSocial(String nombreORazonSocial) {
        this.nombreORazonSocial = nombreORazonSocial;
    }

    public String getRFC() {
        return RFC;
    }

    public void setRFC(String RFC) {
        this.RFC = RFC;
    }

    public String getCP() {
        return CP;
    }

    public void setCP(String CP) {
        this.CP = CP;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }

    public String getMunicipio() {
        return Municipio;
    }

    public void setMunicipio(String Municipio) {
        this.Municipio = Municipio;
    }

}
