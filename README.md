# Español #
# LEAME #

Este proyecto ha sido configurado como se muestra en [Mi blog](http://correctmycode.blogspot.mx/2014/08/programar-colaborativanmente-con-varios.html) (En español por supuesto).

### ¿Para qué es este repositorio? ###

* Resúmen rápido

Este repositorio contiene un sistema para administración de un restaurant

* Versión

0.1

* [Aprender lenguaje Markdown](https://bitbucket.org/tutorials/markdowndemo)

### ¿Cómo lo configuro? ###

* Resumen de preparativos

Tener Java 1.6 o superior
Tener Netbeans IDE

* Configuración
* Dependencias
* Configuración de base de datos
* Como correr las pruebas
* Instrucciones de despliegue

### Lineamientos de contribución ###

* Escritura de pruebas
* Revisión de código



* Otros lineameintos

### ¿Con quién me comunico? ###

* Dueño o administrador del repositorio
  *[Ruslan López Carro](http://www.fb.com/javatlacati)
* Contactar a otros miembros de la comunidad

# English #
# README #

This project has been configured as shown on [My blog](http://correctmycode.blogspot.mx/2014/08/programar-colaborativanmente-con-varios.html) (In spanish).

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
  *[Ruslan López Carro](http://www.fb.com/javatlacati)
* Other community or team contact